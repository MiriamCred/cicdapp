import { Component } from '@angular/core';

@Component({
  selector: 'app-little-tour',
  template: `
  <input  #newHero (keyup.enter) = "addHero(newHero.value)"
  (blur)="addHero(newHero.value); newHero.value='' ">
  <button (click)="addHero(newHero.value)">Add</button>
  <ul><li *ngFor="let hero of heroes">{{hero}}</li></ul>
  `,
  styleUrls: ['./little-tour.component.css']
})

export class LittleTourComponent  {
heroes = ['The Flash','Green Arrow','SuperGirl','SuperMan']

addHero(newHero:string){
  if(newHero){
    this.heroes.push(newHero);
  }
}

}
