import { Component } from '@angular/core';

@Component({
  selector: 'app-keyup',
  template: `
  <input (keyup) ="onKey($event)">
  <p>{{values}}</p>
  <input (keyup) ="onKey1($event)">
  <p>{{values1}}</p>
  `,
  styleUrls: ['./keyup.component.css']
})
export class KeyupComponent {
  values = ''; 
  values1 = '';
  onKey(event: any){
    this.values += event.target.value + ' | ';
  }

  onKey1(event: KeyboardEvent){
    this.values1 += (event.target as HTMLInputElement).value + ' | ';
  }
}

@Component({
  selector: 'app-keyup1',
  template: `
  <input (keyup) ="onKey1($event)">
  <p>{{values1}}</p>
  `,
  styleUrls: ['./keyup.component.css']
})
export class KeyupComponentV1 {
  values1 = '';

  onKey1(event: KeyboardEvent){
    this.values1 += (event.target as HTMLInputElement).value + ' | ';
  }
}

@Component({
  selector: 'app-key-up2',
  template: `
  <input #box (keyup)="onKey2(box.value)" style="color:blue;">
  <p>{{values}}</p>
  `,
})
export class KeyupComponentV2 {
  values = ''; 
  onKey2(value:string){
    this.values += value + ' | ';
  }

}


@Component({
  selector: 'app-key-up3',
  template: `
  <input  #box (keyup.enter) = "onEnter(box.value)">
  <p>{{value}}</p>
  `,
  styleUrls: ['./keyup.component.css']
})
export class KeyupComponentV3 {
  value = '';

  onEnter(value:string){
    this.value = value;
  }
}

@Component({
  selector: 'app-key-up4',
  template: `
  <input  #box (keyup.enter) = "update(box.value)"
  (blur)="update(box.value)">
  <p>{{value}}</p>
  `,
  styleUrls: ['./keyup.component.css']
})
export class KeyupComponentV4 {
  value = '';

  update(value:string){
    this.value = value;
  }
}



