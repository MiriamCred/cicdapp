import { Component, OnInit } from '@angular/core';
import { CartService} from '../cart.service';
import { FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  items;
  checkoutForm;
  constructor(
    /**inject the service, now the cart component can use it */
    private CartService: CartService,
    private formBuilder: FormBuilder
    ){ 
      /**esta linea de codigo permite limpiar los inputs del form and resetearlos */
      this.checkoutForm = this.formBuilder.group({
        name:'',
        address: ''
      })
    }

  ngOnInit(): void {
    this.items = this.CartService.getItems();
  }

  /**funcion de submit */
  onSubmit(customerData)
  {
    //Proceso que revisa los datos
    this.items = this.CartService.clearCart();
    this.checkoutForm.reset();
    console.warn('Your order has been submitted', customerData)
  }

}
