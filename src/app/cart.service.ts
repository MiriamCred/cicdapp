import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  items = [];
  constructor(
    /*se inyecta el httpclient en el servicio cart*/
    private http: HttpClient
  ) { }

  addToCart(product)
  {
    this.items.push(product);
  }

  getItems()
  {
    return this.items;
  }

  clearCart()
  {
    this.items = [];
    return this.items;
  }

  /*Este metodo permite recuperar datos mediante el httpcliente como via desde un archivo json */
  getShippingPrices() {
    return this.http.get('/assets/shipping.json');
  }
}
